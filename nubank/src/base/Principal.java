package base;

import banco.Agencia;
import banco.CCPF;
import banco.CCPJ;
import banco.Poupanca;

public class Principal {

	public static void main(String[] args) {
		Agencia ag = new Agencia("N0123");
	
		CCPJ contaPJ = new CCPJ(1234);
		CCPF contaPF = new CCPF(5235);
		Poupanca poup = new Poupanca(6543);
		if (ag.criarConta(contaPJ)) {
			System.out.println("Conta Criada");
		}
		
		if (ag.criarConta(contaPF)) {
			System.out.println("Conta Criada");
		}
		
		if (ag.criarConta(poup)) {
			System.out.println("Conta Poup Criada");
		}

		contaPJ.setHasChequeEspecial(true);
		contaPJ.setValorChequeEspecial(2000);

		poup.depositar(550);
		contaPJ.depositar(500);
		contaPF.depositar(500);
		
		ag.getContas();
	}

}