package banco;

import java.util.List;

import cliente.Pessoa;

public interface ContaIfc {
	public List<Pessoa> getCorrentista();
	public int getNumero();
	public void vincularCorrentista(Pessoa p);
	public double getSaldo();
	public void sacar(double valor);
	public void depositar(double valor);
	public String getTipoConta();
}
