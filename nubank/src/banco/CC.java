package banco;

import exc.SaldoInsuficiente;

public abstract class CC extends Conta {

	private boolean hasChequeEspecial = false;
	private double valorChequeEspecial = 0;

	public CC(int numeroConta, String tipoConta) {
		super(numeroConta, tipoConta);
	}

	public boolean isHasChequeEspecial() {
		return hasChequeEspecial;
	}

	public void setHasChequeEspecial(boolean hasChequeEspecial) {
		this.hasChequeEspecial = hasChequeEspecial;
	}

	public void setValorChequeEspecial(double valorChequeEspecial) {
		this.valorChequeEspecial = valorChequeEspecial;
	}

	@Override
	public double getSaldo() {
		double saldoEmConta = saldo;
		if (hasChequeEspecial) {
			saldoEmConta += valorChequeEspecial;
			// saldoEmConta = saldoEmConta + valorChequeEspecial;
		}
		return saldoEmConta;
	}

	@Override
	public void sacar(double valor) {
		if (getSaldo() > 0 && valor < getSaldo()) {
			System.out.println("Farra $" + valor);
			saldo -= valor;
		} else {
			new SaldoInsuficiente();
		}
	}

}
