package banco;

public class Poupanca extends Conta {
	public Poupanca(int numeroConta) {
		super(numeroConta, "Conta Poupanca");
	}

	@Override
	public double getSaldo() {
		return saldo;
	}

	@Override
	public void sacar(double valor) {
		if (saldo >0 && valor <= saldo) {
			System.out.println("Saco $" +valor);
			saldo -= valor;
		} else {
			throw new IllegalArgumentException("saldo insuficiente");
		}
	}

}
