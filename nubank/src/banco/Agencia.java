package banco;

import java.util.ArrayList;
import java.util.List;

public class Agencia {
	String numero;
	List<ContaIfc> contas = new ArrayList<ContaIfc>();
	
	public Agencia(String numero) {
		this.numero = numero;
	}
	
	public boolean criarConta(ContaIfc c) {
		contas.add(c);
		return true;
	}

	public List<ContaIfc> getContas() {
		for (ContaIfc c: contas) {
			System.out.println("Conta:" + c.getNumero()  
			+	" Tipo:" + c.getTipoConta()
			+ " Saldo ($):" + c.getSaldo() );
		}
		return contas;
	}
	
	public String getNumero() {
		return numero;
	}
	
	
}
