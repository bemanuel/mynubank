package banco;

import java.util.ArrayList;
import java.util.List;

import cliente.Pessoa;

public abstract class Conta implements ContaIfc {
	List<Pessoa> correntistas = new ArrayList<Pessoa>();
	int numeroConta = 0;
	double saldo = 0;
	
	String tipoConta= "";
	
	public Conta(int numeroConta, String tipoConta) {
		this.numeroConta = numeroConta;
		this.tipoConta = tipoConta;
	}
	
	
	public String getTipoConta() {
		return tipoConta;
	}
	
	@Override
	public int getNumero() {
		return numeroConta;
	}

	@Override
	public List<Pessoa> getCorrentista() {
		return correntistas;
	}

	@Override
	public void vincularCorrentista(Pessoa p) {
		if (correntistas.size() <= 3) {
			correntistas.add(p);
		} else {
			throw new IllegalArgumentException("Atingiu o limite de 3 correntistas");
		}
	}

	@Override
	public abstract double getSaldo();

	@Override
	public abstract void sacar(double valor);

	@Override
	public void depositar(double valor) {
		if (valor > 0) {
			saldo += valor;
			System.out.println("Deposito:" + valor 
					+ "\t Saldo atual: " + getSaldo());
		
		} else {
			throw new IllegalArgumentException("deposito negativo");
		}
	}

}
