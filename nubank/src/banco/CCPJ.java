package banco;

import cliente.PJ;

public class CCPJ extends CC {

	private PJ empresa;
	
	public CCPJ(int numeroConta) {
		super(numeroConta, "Conta-Corrente PJ");
	}

	
	public PJ getEmpresa() {
		return empresa;
	}
	
	public void setEmpresa(PJ empresa) {
		this.empresa = empresa;
	}
	
}
