package cliente;

public interface PessoaIfc {
	String getNome();
	void setNome(String nome);
	String getEndereco();
	void setEndereco(String endereco);
}