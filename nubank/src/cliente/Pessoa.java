package cliente;

public class Pessoa implements PessoaIfc  {
	private String nome;
	private String endereco;
	@Override
	public String getNome() {
		return nome;
	}
	@Override
	public void setNome(String nome) {
		this.nome = nome;
	}
	@Override
	public String getEndereco() {
		return endereco;
	}
	@Override
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	
}
